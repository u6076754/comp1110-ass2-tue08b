package comp1110.ass2.gui;

// The classes updates the board graphically according to the moves through the use of java FX
// This class also creates the visuals which we been completed later to give the a user friendly visuals and interface for the game

import comp1110.ass2.TwistGame;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;


//Millie

// FIXME Task 7: Implement a basic playable Twist Game in JavaFX that only allows pieces to be placed in valid places

// FIXME Task 8: Implement starting placements

// FIXME Task 10: Implement hints

// FIXME Task 11: Generate interesting starting placements

public class Viewer extends Application {
    /*grid stuff*/
    private static final int ROWS = 4;
    private static final int COLUMNS = 8;

    private final Group grid = new Group();


    /* board layout */
    private static final double SQUARE_SIZE = 60;
    private static final int VIEWER_WIDTH = 750;
    private static final int VIEWER_HEIGHT = 500;

    private static final String URI_BASE = "assets/";

    private final Group root = new Group();
    private final Group controls = new Group();

    TextField textField;


    //adds a placement
    void makePlacements(String placements) {
        System.out.println(String.valueOf(TwistGame.isPlacementStringValid(placements)));

        //clears past placement
        root.getChildren().clear();
        root.getChildren().add(controls);

        //error
        if(!TwistGame.isPlacementStringValid(placements) || !TwistGame.isPlacementStringWellFormed(placements)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("invalid placement string");
            alert.setContentText("check to make sure your string has correct placements");
            alert.showAndWait();
        }
        else {

            int insetX = (int) ((VIEWER_WIDTH - (COLUMNS * SQUARE_SIZE)) / 2);
            int insetY = (int) ((VIEWER_HEIGHT - (ROWS * SQUARE_SIZE)) / 2);
            //vertical lines
            for(int k = 0; k <= COLUMNS; k++){

                final Line verticalLine = new Line((k * SQUARE_SIZE) + insetX,insetY,(k * SQUARE_SIZE) + insetX, (VIEWER_HEIGHT - insetY));
                controls.getChildren().add(verticalLine);

            }
            //horizontal lines
            for(int j = 0; j <= ROWS; j++){

                final Line horizontalLine = new Line(insetX,(j * SQUARE_SIZE) + insetY, (VIEWER_WIDTH - insetX), (j * SQUARE_SIZE) + insetY);
                controls.getChildren().add(horizontalLine);
            }

            //set targets for drag and drop
            root.setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    boolean success = false;

                    Dragboard db = event.getDragboard();

                    String oldPlacement = db.getString();
                    String newPlacement;

                    int imageY = (int) event.getScreenY();
                    int row = (int) ((imageY - insetY * 2) % SQUARE_SIZE);

                    int imageX = (int) event.getScreenY();
                    int column = Math.abs (imageX - (int) ((imageX - insetX * 2) % SQUARE_SIZE));

                    newPlacement = oldPlacement.charAt(0) + column + String.valueOf((int) row) + oldPlacement.charAt(3);

                    String newPlacements = placements.replaceAll(oldPlacement, newPlacement);
                    if (TwistGame.isPlacementStringValid(newPlacements)){
                        event.acceptTransferModes(TransferMode.MOVE);
                        success = true;
                    }

                }
            });

            root.setOnDragDropped(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    boolean success = false;

                    Dragboard db = event.getDragboard();

                    String oldPlacement = db.getString();
                    String newPlacement;

                    int imageY = (int) event.getScreenY();
                    int row = (int) ((imageY - insetY * 2) % SQUARE_SIZE);

                    int imageX = (int) event.getScreenY();
                    int column = Math.abs (imageX - (int) ((imageX - insetX * 2) % SQUARE_SIZE));

                    newPlacement = oldPlacement.charAt(0) + column + String.valueOf((int) row) + oldPlacement.charAt(3);

                    String newPlacements = placements.replaceAll(oldPlacement, newPlacement);
                    if (TwistGame.isPlacementStringValid(newPlacements)){
                        event.acceptTransferModes(TransferMode.MOVE);
                        success = true;
                        makePlacements(newPlacements);
                    }

                    event.setDropCompleted(success);

                    event.consume();

                }
            });


            //displays pieces
            for (int i = 0; i < placements.length(); i += 4) {
                String placement = placements.substring(i, i + 4);

                //Image image = new Image(new FileInputStream("path of the image"));
                char shape = placement.charAt(0);
                int x = Character.getNumericValue(placement.charAt(1));
                int y = Character.getNumericValue(placement.charAt(2)) - 10;
                int rotation = Character.getNumericValue(placement.charAt(3));

                //add the correct shape
                Image image = new Image(getClass().getResource(URI_BASE + shape + ".png").toString());
                ImageView iv1 = new ImageView();
                iv1.setImage(image);

                //change rotation/reflection of the piece
                int r = rotation;
                if(r >= 4)r -= 4;
                iv1.setRotate(r * 90);
                if(rotation >= 4)iv1.setScaleY(-1);

                //change scale with hardcoding of the pieces
                int width;
                int height;

                //set width for unreflected
                if (shape == 'a'||shape == 'b'|| shape == 'd' || shape == 'f' || shape == 'g' || shape == 'h') {
                    width = 3;
                } else if (shape == 'c'){
                    width = 4;
                } else if (shape == 'g' || shape == 'e'){
                    width = 2;
                }else {
                    width = 1;
                }

                //set height for unreflected
                if(shape == 'a'||shape == 'b'|| shape == 'd' || shape == 'e' || shape == 'f'){
                    height = 2;
                }else if (shape == 'c' || shape == 'h'){
                    height = 1;
                }else if (shape == 'g'){
                    height = 3;
                }else height = 1;


                //adjust images to be in line with square size
                int widthScale = (int) (width * SQUARE_SIZE);
                int heightScale = (int) (height * SQUARE_SIZE);

                //adjust image view in accordance to height and width
                iv1.setFitHeight(heightScale);
                iv1.setFitWidth(widthScale);

                if(rotation % 2 == 0 || height == width){
                    //change position based on x and y coordinates for no rotation with change in side lengths
                    iv1.setTranslateX(SQUARE_SIZE * (x-1) + insetX);
                    iv1.setTranslateY(SQUARE_SIZE * y + insetY);
                }else {
                    //change position based on x and y coordinates for rotation with change in side lengths
                    iv1.setTranslateX(SQUARE_SIZE * (x-1) + insetX - (((double) (Math.abs(height-width)) / 2) * SQUARE_SIZE));
                    iv1.setTranslateY(SQUARE_SIZE * y + insetY + (((double) (Math.abs(height-width)) / 2) * SQUARE_SIZE));
                }

                root.getChildren().add(iv1);

                iv1.setOnDragDetected(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        Dragboard db = iv1.startDragAndDrop(TransferMode.MOVE);

                        ClipboardContent clipboardContent = new ClipboardContent();
                        clipboardContent.putString(placement);

                        //db.setDragView(Image.impl_fromPlatformImage(iv1));

                        event.consume();

                    }
                });

            }
        }

    }

    /**
     * Create a basic text field for input and a refresh button.
     */

    private void makeControls() {
        TextArea textArea = new TextArea("invalid placement");
        textArea.setEditable(false);
        textArea.setWrapText(true);

        Label label1 = new Label("Placement:");
        textField = new TextField ();
        textField.setPrefWidth(300);
        Button button = new Button("Refresh");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                makePlacements(textField.getText());
                textField.clear();
            }
        });

        HBox hb = new HBox();
        hb.getChildren().addAll(label1, textField, button);
        hb.setSpacing(10);
        hb.setLayoutX(130);
        hb.setLayoutY(VIEWER_HEIGHT - 50);
        controls.getChildren().add(hb);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("TwistGame Viewer");
        Scene scene = new Scene(root, VIEWER_WIDTH, VIEWER_HEIGHT);
        makeControls();
        root.getChildren().add(controls);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

