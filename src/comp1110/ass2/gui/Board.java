/**
 * Created by millie on 19/10/18.
 */
package comp1110.ass2.gui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.image.Image;

/**
 * A very simple viewer for piece placements in the twist game.
 *
 * NOTE: This class is separate from your main game class.  This
 * class does not play a game, it just illustrates various piece
 * placements.
 */
public class Board extends Application {

    /*grid stuff*/
    private static final int ROWS = 4;
    private static final int COLUMNS = 8;

    /* board layout */
    private static final double SQUARE_SIZE = 60;
    private static final int VIEWER_WIDTH = 750;
    private static final int VIEWER_HEIGHT = 500;

    private static final String URI_BASE = "assets/";

    private final Group root = new Group();
    private final Group controls = new Group();
    TextField textField;

    /**
     * Draw a placement in the window, removing any previously drawn one
     *
     * @param placement  A valid placement string
     */
    //Millie
    void makePlacement(String placement) {
        // FIXME Task 4: implement the simple placement viewer
        root.getChildren().clear();
        root.getChildren().add(controls);

        int insetX = (int) ((VIEWER_WIDTH - (COLUMNS * SQUARE_SIZE)) / 2);
        int insetY = (int) ((VIEWER_HEIGHT - (ROWS * SQUARE_SIZE)) / 2);

        //vertical lines
        for(int i = 0; i <= COLUMNS; i++){
            Line verticalLine = new Line((i * SQUARE_SIZE) + insetX,insetY,(i * SQUARE_SIZE) + insetX, (VIEWER_HEIGHT - insetY));
            controls.getChildren().add(verticalLine);
        }
        //horizontal lines
        for(int i = 0; i <= ROWS; i++){
            Line horizontalLine = new Line(insetX,(i * SQUARE_SIZE) + insetY, (VIEWER_WIDTH - insetX), (i * SQUARE_SIZE) + insetY);
            controls.getChildren().add(horizontalLine);
        }

        //Image image = new Image(new FileInputStream("path of the image"));
        char shape = placement.charAt(0);
        int x = Character.getNumericValue(placement.charAt(1));
        int y = Character.getNumericValue(placement.charAt(2)) - 10;
        int rotation = Character.getNumericValue(placement.charAt(3));

        //add the correct shape
        Image image = new Image(getClass().getResource(URI_BASE + shape + ".png").toString());
        ImageView iv1 = new ImageView();
        iv1.setImage(image);

        //change rotation/reflection of the piece
        int r = rotation;
        if(r >= 4)r -= 4;
        iv1.setRotate(r * 90);
        if(rotation >= 4)iv1.setScaleY(-1);

        //change scale
        int width;
        int height;

        //set width for unreflected
        if (shape == 'a'||shape == 'b'|| shape == 'd' || shape == 'f' || shape == 'g' || shape == 'h') {
            width = 3;
        } else if (shape == 'c'){
            width = 4;
        } else width = 2;

        //set height for unreflected
        if(shape == 'a'||shape == 'b'|| shape == 'd' || shape == 'e' || shape == 'f'){
            height = 2;
        }else if (shape == 'c' || shape == 'h'){
            height = 1;
        }else {
            height = 3;
        }
        //adjust to factor in square size
        int widthScale = (int) (width * SQUARE_SIZE);
        int heightScale = (int) (height * SQUARE_SIZE);

        //adjust image view in accordance to height and width
        iv1.setFitHeight(heightScale);
        iv1.setFitWidth(widthScale);

        //change position based on x and y coordinates for no reflection
        iv1.setX(SQUARE_SIZE * x + insetX - SQUARE_SIZE);
        iv1.setY(SQUARE_SIZE * y + insetY);

        //change position based on x and y coordinates for reflection
        if(rotation % 2 != 0){
            iv1.setX(SQUARE_SIZE * x + insetX - SQUARE_SIZE - (widthScale/2));
            iv1.setY(SQUARE_SIZE * y + insetY + (SQUARE_SIZE/2));
        }

        root.getChildren().add(iv1);

    }

    /**
     * Create a basic text field for input and a refresh button.
     */
    private void makeControls() {
        Label label1 = new Label("Placement:");
        textField = new TextField ();
        textField.setPrefWidth(300);
        Button button = new Button("Refresh");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                makePlacement(textField.getText());
                textField.clear();
            }
        });
        HBox hb = new HBox();
        hb.getChildren().addAll(label1, textField, button);
        hb.setSpacing(10);
        hb.setLayoutX(130);
        hb.setLayoutY(VIEWER_HEIGHT - 50);
        controls.getChildren().add(hb);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("TwistGame Viewer");
        Scene scene = new Scene(root, VIEWER_WIDTH, VIEWER_HEIGHT);
        makeControls();
        root.getChildren().add(controls);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
