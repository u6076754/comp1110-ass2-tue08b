package comp1110.ass2.gui;

public class Placement {
    // This class represents the legitimacy of the tile movement, whether is can be placed in certain position whilst abiding by the given rules
    // Within this the board and the specified rules and regulations are written which govern the placement
    // Essentially this is giving the objective of the game


    private static final int BOARD_WIDTH = 933;
    private static final int BOARD_HEIGHT = 700;

    //this represents the board in terms of each individual cells state - e.g. if it has peg, piece, empty
    // Hence breaking apart the given string into to components
    /*
    1 = empty
    2 = peg
    3 = hole
    4 = filled in piece
    5 = peg + hole
     */
    int[][] boardState = new int[8][4];

    //represents the colour of each cell in board - will be altered in alter methods
    int[][] boardColour = new int[8][4];


    //this will alter the pegBoard with the placement string to add pegs
    //will also alter boardColour w peg information
    public void pegAddition(String placementString){ }




    //checks if there is peg, hence checking legality of the move
    public boolean peg(int[][] boardColour, int[][] boardState, int x, int y) {
        if((boardState[x][y] == 2) || (boardState[x][y] == 5)) return true;
        return false ;
    }

    // Check that all of the piece lies upon the given board



    //takes a piece and position and updates boardState and boardColour with the new move
    //Hence updating the new string in for the game
    public void updateBoard (Placement placement, int x, int y){
    }

    //Repeat the above process till all pieces are used, or a dead end has been reached where there is no solutions
    //Then return output the solved board solution and therefore solved string

}
