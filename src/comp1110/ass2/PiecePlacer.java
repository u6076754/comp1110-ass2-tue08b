package comp1110.ass2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PiecePlacer {

    //Peter.Cominos
    //This class places the piece onto the board as if it has no holes
    //The board is fashioned in number 1 through 32 for ease

    //places each piece onto the board in such a way that each piece fill a certain number the 32 possible options, hence dividing the board up into a 8 * 4 grid
    static List<Integer> piecePlacer(String placement) {
        int stringLength = placement.length();
        List<Integer> listOfFilledNumbers = new ArrayList<>();

        for (int i = 0; i < stringLength; i += 4) {
            char oneString = placement.charAt(i); //Which shape
            char twoString = placement.charAt(i + 1); //Which Column
            char threeString = placement.charAt(i + 2); // Which Row
            char fourString = placement.charAt(i + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString;
            int fourInt = fourString - '0';

            int shifter = (twoInt - 1) + ((threeInt - 65) * 8);


            //Adding each pieces individual squares to the Array list  -- the pegs are excluded as they would create overlaps
            if (oneString == 'a') {
                if (fourInt == 0) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 1) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);listOfFilledNumbers.add(17 + shifter);
                } else if (fourInt == 2) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 3) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(17 + shifter);
                } else if (fourInt == 4) {
                    listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(3 + shifter);
                } else if (fourInt == 5) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(17 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);
                } else if (fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);
                }
            } else if (oneString == 'b') {
                if (fourInt == 0 || fourInt == 2) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 1 || fourInt == 3) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(17 + shifter);
                } else if (fourInt == 4 || fourInt == 6) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 5 || fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);
                }
            } else if (oneString == 'c') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(4 + shifter);
                } else if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(17 + shifter);listOfFilledNumbers.add(25 + shifter);
                }
            } else if (oneString == 'd') {
                if (fourInt == 0) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 1) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(17 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 2) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 3) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(17 + shifter);
                } else if (fourInt == 4) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 5) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(17 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);
                }
            } else if (oneString == 'e') {
                if (fourInt == 0 || fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 1 || fourInt == 4) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 2 || fourInt == 5) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 3 || fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);
                }
            } else if (oneString == 'f') {
                if (fourInt == 0 || fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(10 + shifter);
                } else if (fourInt == 1 || fourInt == 7) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 2 || fourInt == 4) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);
                } else if (fourInt == 3 || fourInt == 5) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(17 + shifter);
                }
            } else if (oneString == 'g') {
                if (fourInt == 0) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 1) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 2) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(19 + shifter);
                } else if (fourInt == 3) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(17 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 4) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(17 + shifter);
                } else if (fourInt == 5) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 6) {
                    listOfFilledNumbers.add(3 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(11 + shifter);listOfFilledNumbers.add(18 + shifter);
                } else if (fourInt == 7) {
                    listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(10 + shifter);listOfFilledNumbers.add(18 + shifter);listOfFilledNumbers.add(19 + shifter);
                }
            } else if (oneString == 'h') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(2 + shifter);listOfFilledNumbers.add(3 + shifter);
                } else if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) {
                    listOfFilledNumbers.add(1 + shifter);listOfFilledNumbers.add(9 + shifter);listOfFilledNumbers.add(17 + shifter);
                }
            }
        }
        Collections.sort(listOfFilledNumbers);
        return listOfFilledNumbers;
    }
}
