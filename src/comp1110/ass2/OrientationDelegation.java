package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;

public class OrientationDelegation {

    // Peter.Cominos
    //This class checks whether the orientation of the piece has been double counted

    static List<String> orientationDeleter(List<String> list) {
        List<String> newList = new ArrayList<>(list);
        List<String> addedLIst = new ArrayList<>();

        int length = newList.size();

        for (int i = 0; i < length; i ++){
            String toBeTested = newList.get(i);
            char one = toBeTested.charAt(0);
            char two = toBeTested.charAt(1);
            char three = toBeTested.charAt(2);
            char four = toBeTested.charAt(3);

            if (i > 0) {
                String toBeCompared = newList.get(i - 1);
                char five = toBeCompared.charAt(0);
                char six = toBeCompared.charAt(1);
                char seven = toBeCompared.charAt(2);
                char eight = toBeCompared.charAt(3);

                if (one == 'c' && four >= '4') {
                    continue;
                } else if (one == 'h' && four >= '4'){
                    continue;
                } else if (one != five){
                    addedLIst.add(toBeTested);
                } else if (one =='b' && two == '6' && three == 'A' && four == '7') {
                    addedLIst.add("b6A7");
                } else if (one == 'b') {
                    if (four == '2' ) {
                        addedLIst.add("" + 'b' + toBeTested.charAt(1)+ toBeTested.charAt(2) +  '0');
                    } else if (four == '3') {
                        addedLIst.add("" + 'b' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "1");
                    } else if (four == '6'){
                        addedLIst.add("" + 'b' + toBeTested.charAt(1)+ toBeTested.charAt(2) + '4');
                    } else if (four == '7'){
                        addedLIst.add("" + 'b' + toBeTested.charAt(1)+ toBeTested.charAt(2) +  "5");
                    }
                } else if (one == 'e') {
                    if(four == '7') {
                        addedLIst.add("" + 'e' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "0");
                    } else if(four == '4') {
                        addedLIst.add("" + 'e' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "1");
                    } else if(four == '5') {
                        addedLIst.add("" + 'e' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "2");
                    } else if(four == '6') {
                        addedLIst.add("" + 'e' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "3");
                    }
                } else if (one == 'f') {
                    if(four == '6') {
                        addedLIst.add("" + 'f' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "0");
                    } else if(four == '7') {
                        addedLIst.add("" + 'f' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "1");
                    } else if(four == 4) {
                        addedLIst.add("" + 'f' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "2");
                    } else if(four == '5') {
                        addedLIst.add("" + 'f' + toBeTested.charAt(1)+ toBeTested.charAt(2) + "3");
                    }
                } else if (one == 'a' || one == 'd' || one == 'g') {
                    addedLIst.add(toBeTested);
                } else if (one == five && two != six) {
                    addedLIst.add(toBeTested);
                }else if (one == five && two == six && three != seven) {
                    addedLIst.add(toBeTested);
                }

            } else if (i == 0) {
                addedLIst.add(toBeTested);

            }

        }

        return addedLIst;

    }
}
