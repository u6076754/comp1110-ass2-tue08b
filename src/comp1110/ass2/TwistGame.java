package comp1110.ass2;

import java.lang.reflect.Array;
import java.util.*;

/**
 * This class provides the text interface for the Twist Game
 * <p>
 * The game is based directly on Smart Games' IQ-Twist game
 * (http://www.smartgames.eu/en/smartgames/iq-twist)
 */
public class TwistGame {

  /**
   * Determine whether a piece or peg placement is well-formed according to the following:
   * - it consists of exactly four characters
   * - the first character is in the range a .. l (pieces and pegs)
   * - the second character is in the range 1 .. 8 (columns)
   * - the third character is in the range A .. D (rows)
   * - the fourth character is in the range 0 .. 7 (if a piece) or is 0 (if a peg)
   *
   * @Peter.Cominos
   * @param piecePlacement A string describing a single piece or peg placement
   * @return True if the placement is well-formed
   */
  public static boolean isPlacementWellFormed(String piecePlacement) {
    //Break the string into an array of chars
    char [] list = piecePlacement.toCharArray();
    if (list.length == 4) {
      char one = list[0];
      char two = list[1];
      int numberTwo = (int) two;
      char three = list[2];
      char four = list[3];
      int numberFour = (int) four;

      if (one < 'a' || one > 'l') {
        return false;
      } else if (three < 'A' || three > 'D') {
        return false;
      } else if (numberTwo < 49 || numberTwo > 56) { //use of numeric values representing letters for ease
        return false;
      } else if (numberFour < 48 || numberFour > 55) {
        return false;
      } else if (one == 'i' && numberFour != 48) {
        return false;
      }else if (one == 'j' && numberFour != 48) {
        return false;
      }else if (one == 'k' && numberFour != 48) {
        return false;
      } else if (one == 'l' && numberFour != 48) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
    // FIXME Task 2: determine whether a piece or peg placement is well-formed

  /**
   * Determine whether a placement string is well-formed:
   * - it consists of exactly N four-character piece placements (where N = 1 .. 15);
   * - each piece or peg placement is well-formed
   * - each piece or peg placement occurs in the correct alphabetical order (duplicate pegs can be in either order)
   * - no piece or red peg appears more than once in the placement
   * - no green, blue or yellow peg appears more than twice in the placement
   *
   * @param placement A string describing a placement of one or more pieces and pegs
   * @return True if the placement is well-formed
   * @peterCominos
   */
  public static boolean isPlacementStringWellFormed(String placement) {
      char [] letters = placement.toCharArray();
      int length = letters.length;

      if (length % 4 == 0) {
          if (length/4 < 1 || length/4 > 15) {
              return false;
          } if (!orderChecker(letters)){
              return false;
          } if (testduplicates(placement, 'a') > 1) {
              return false;
          } if (testduplicates(placement, 'b') > 1) {
              return false;
          }if (testduplicates(placement, 'c') > 1) {
              return false;
          }if (testduplicates(placement, 'd') > 1) {
              return false;
          }if (testduplicates(placement, 'e') > 1) {
              return false;
          }if (testduplicates(placement, 'f') > 1) {
              return false;
          }if (testduplicates(placement, 'g') > 1) {
              return false;
          }if (testduplicates(placement, 'h') > 1) {
              return false;
          }if (testduplicates(placement, 'i') > 1) {
              return false;
          }if (testduplicates(placement, 'j') > 2) {
              return false;
          }if (testduplicates(placement, 'k') > 2) {
              return false;
          }if (testduplicates(placement, 'l') > 2) {
              return false;
          } if (!testStringFormality(placement)) {
              return false;
          } else {
              return true;
          }
          } return false;
      }

      // FIXME Task 3: determine whether a placement is well-formed


// this is for checking the pieces order
  static Boolean orderChecker(char [] x) {
      int length = x.length;
      char [] everyFourthLetter = new char[length/4];
      int index = -1;
      for (int i = 0; i < x.length; i +=4) {
          index ++;
          everyFourthLetter [index] = x[i];
      }
      char [] cloneArray = everyFourthLetter.clone();
      Arrays.sort(everyFourthLetter);

      if (!tester(everyFourthLetter, cloneArray)) {
          return false;
      } else {
          return true;
      }
  }

// this is for checking the validity of the alphabetic order in the pieces check
    static boolean tester(char [] a, char[] b) {
        for (int i = 0; i < 2; i++) {
            if (a[i] != b[i]) {
                return false;
            } else {
                return true;
            }
        }return false;
    }
    // counter of the number of time a char occurs in a string
    static int testduplicates(String x, char y) {
      int counter = 0;
       for (int i = 0; i < x.length(); i ++) {
           if (x.charAt(i) == y) {
               counter ++;
           }
       }
       return counter;
    }

//testing whether the piece if well formed

    static boolean testStringFormality (String x) {
        char [] y = x.toCharArray();
        char [] exemplar = new char [4];
        for (int i = 0; i < y.length; i+= 4 ) {
            String z;
            exemplar[0] = x.charAt(i);
            exemplar[1] = x.charAt(i + 1);
            exemplar[2] = x.charAt(i +2);
            exemplar[3] = x.charAt(i +3);
            z = ""+exemplar[0]+exemplar[1]+exemplar[2]+exemplar[3];
            if (!isPlacementWellFormed(z)) {
                return false;
            }
        }
        return true;
    }

  /**
   * Determine whether a placement string is valid.  To be valid, the placement
   * string must be well-formed and each piece placement must be a valid placement
   * according to the rules of the game.
   * - pieces must be entirely on the board -- Not Working -- Deals with through creating a box around each Shape
   * - pieces must not overlap each other -- Done -- Creating a grid of 32 Square and checking if each are filled previously
   * - pieces may only overlap pegs when the a) peg is of the same color and b) the
   *   point of overlap in the piece is a hole. -- Done be re-hardcoding for the holes in pieces
   *
   * @Peter.Cominos
   * @param placement A placement sequence string
   * @return True if the placement sequence is valid
   */
  public static boolean isPlacementStringValid(String placement) {
        if (doesPlacementNotOverlap(placement)  && arePegsCorrect(placement) && OnBoard.isPlacementOnBoard(placement) ) {//  //assessing where the string being implemented is valid   arePegsCorrect(placement) &&
          return true;
        } else
          return false;

    // FIXME Task 5: determine whether a placement string is valid
  }

//Checking each individual square that there is no more then one piece in each square by checking if there is no more than one of each number
static Boolean doesPlacementNotOverlap(String placement)  {
      PiecePlacer.piecePlacer(placement);
      int listLength = PiecePlacer.piecePlacer(placement).size();

      for (int i = 0; i < listLength - 1; i ++) {
            int nextNumber = i + 1;
            if (PiecePlacer.piecePlacer(placement).get(i).equals(PiecePlacer.piecePlacer(placement).get(nextNumber))) { return false; }
      } return true;

}

static Boolean arePegsCorrect (String placement) {
    int stringLength = placement.length();

    // testing each individual peg against a board of its own to ensure that it has a place
    for (int i = 0; i < stringLength; i += 4) {
        char onePeg = placement.charAt(i); //Which shape

        if (onePeg == 'i') { // red
            List<Integer> listOfI = PeChecker.iBoardChecker(placement);
            for (int b = 0; b < listOfI.size() - 1; b ++) {
                if (listOfI.get(b).equals(listOfI.get(b + 1))) {
                    return false; } }
        } else if (onePeg == 'j') { // blue
            List <Integer> listOfJ = PeChecker.jBoardChecker(placement);
           for (int b = 0; b < listOfJ.size() - 1; b ++) {
                if (listOfJ.get(b).equals(listOfJ.get(b + 1))) { return false; } }
        } else if (onePeg == 'k') { // green
            List <Integer> listOfK = PeChecker.kBoardChecker(placement);
            for (int b = 0; b < listOfK.size() - 1; b ++) {
                if (listOfK.get(b).equals(listOfK.get(b + 1))) { return false; }
            }
        } else if (onePeg == 'l') { // yellow
            List <Integer> listOfL = PeChecker.lBoardChecker(placement);
            for (int b = 0; b < listOfL.size() - 1; b ++) {
                if (listOfL.get(b).equals(listOfL.get(b + 1))) { return false; }
            }
        }
    } return true;
}
  /**
   * Given a string describing a placement of pieces and pegs, return a set
   * of all possible next viable piece placements.   To be viable, a piece
   * placement must be a valid placement of a single piece.  The piece must
   * not have already been placed (ie not already in the placement string),
   * and its placement must be valid.   If there are no valid piece placements
   * for the given placement string, return null.
   *
   * When symmetric placements of the same piece are viable, only the placement
   * with the lowest rotation should be included in the set.
   *
   * @param placement A valid placement string (comprised of peg and piece placements)
   * @return An set of viable piece placements, or null if there are none.
   * @peter.cominos
   */
  public static Set<String> getViablePiecePlacements(String placement) {
      if(allvariablesfinder(placement).isEmpty()) {
          return null;
      } else return allvariablesfinder(placement);
    // FIXME Task 6: determine the set of valid next piece placements
  }

  static Set<String> allvariablesfinder(String placment) {
      List<String> variableList = new ArrayList<>();

      for (int i = 97; i < 105; i ++) {  //This finds all 2048 random combinations of the available letters and numbers
          for (int j = 0; j < 8; j++) {
              for (int k = 65; k < 69; k++) {
                  for (int l = 0 ; l < 8 ; l++) {
                      char identifier = (char)i;
                      String column = ("" + j);
                      char row = (char)k;
                      String orientation = ("" + l);
                      String tobeAdded =  identifier + column + row + orientation;
                      variableList.add(tobeAdded);
                  }
              }
          }
      }

      List<String> extraFinalList = new ArrayList<>();

      for (int y = 0; y < 2048 ; y ++) {  //checks the placement and validity of all the 2048 piece combinations when inserted into the string
          List<String> finalList = new ArrayList<>();

          String str = placment;
          char [] chars = str.toCharArray();

          String piece = variableList.get(y);
          char chr = piece.toCharArray()[0]; // gets first letter of string

          String newStr = "";

          boolean filled = false;
          for (int x = 0; x < str.length(); x += 4) {
              if (chars[x] == chr)
                  finalList.add(str) ;

              if (chars[x] > chr) {
                  newStr = str.substring(0, x)
                          + piece + str.substring(x, str.length());
                  filled = true;
                  break;
              }
          }
          if (! filled){
              newStr = str + piece;
          }
          finalList.add(newStr);

          String totest = finalList.get(0);

          if (isPlacementStringValid(totest) && totest != placment && isPlacementStringWellFormed(totest)) {
              extraFinalList.add(piece);
          }
      }
      Set<String>  theList= new HashSet<>(OrientationDelegation.orientationDeleter(extraFinalList)); //Removes an unwanted symmetrical pieces
      return theList;
  }

  /**
   * Return an array of all unique solutions for a given starting placement.
   *
   * Each solution should be a 32-character string giving the placement sequence
   * of all eight pieces, given the starting placement.
   *
   * The set of solutions should not include any symmetric piece placements.
   *
   * In the IQ-Twist game, valid challenges can have only one solution, but
   * other starting placements that are not valid challenges may have more
   * than one solution.  The most obvious example is the unconstrained board,
   * which has very many solutions.
   *
   * @param placement A valid piece placement string.
   * @return An array of strings, each 32-characters long, describing a unique
   * unordered solution to the game given the starting point provided by placement.
   */
  public static String[] getSolutions(String placement) {
    // FIXME Task 9: determine all solutions to the game, given a particular starting placement
    return null;
  }
}
