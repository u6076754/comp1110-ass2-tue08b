package comp1110.ass2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Peter.Cominos
//This class checks the pes are in the right positions for the corresponding board

public class PeChecker {
    static List iBoardChecker(String placement) {
        int stringLength = placement.length();
        List<Integer> iList = new ArrayList<>();

        for (int a = 0; a < stringLength; a +=4) {
            char oneString = placement.charAt(a); //Which shape
            char twoString = placement.charAt(a + 1); //Which Column
            char threeString = placement.charAt(a + 2); // Which Row
            char fourString = placement.charAt(a + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString;
            int fourInt = fourString - '0';
            int shifter = (twoInt - 1) + ((threeInt - 65) * 8);

            if (oneString == 'c' || oneString == 'd' || oneString == 'e' || oneString == 'f' || oneString == 'g' || oneString == 'h') {
                String send = "" + oneString + twoString + threeString + fourString;
                iList.addAll(PiecePlacer.piecePlacer(send));
            } else if (oneString == 'a') {
                if (fourInt == 0) {
                    iList.add(2 + shifter);iList.add(11 + shifter);
                } else if (fourInt == 1) {
                    iList.add(10 + shifter);iList.add(17 + shifter);
                } else if (fourInt == 2) {
                    iList.add(1 + shifter);iList.add(10 + shifter);
                } else if (fourInt == 3) {
                    iList.add(2 + shifter);iList.add(9 + shifter);
                } else if (fourInt == 4) {
                    iList.add(10 + shifter);iList.add(3 + shifter);
                } else if (fourInt == 5) {
                    iList.add(9 + shifter);iList.add(18 + shifter);
                } else if (fourInt == 6) {
                    iList.add(2 + shifter);iList.add(9 + shifter);
                } else if (fourInt == 7) {
                    iList.add(1 + shifter);iList.add(10 + shifter);
                }
            } else if (oneString == 'b') {
                if (fourInt == 0 ) {
                    iList.add(1 + shifter);iList.add(2 + shifter);iList.add(11 + shifter);
                } else if (fourInt == 1 ) {
                    iList.add(2 + shifter);iList.add(10 + shifter);iList.add(17 + shifter);
                } else if (fourInt == 4 ) {
                    iList.add(3 + shifter);iList.add(9 + shifter);iList.add(10 + shifter);
                } else if (fourInt == 5 ) {
                    iList.add(1 + shifter);iList.add(9 + shifter);iList.add(18 + shifter);
                } else if ( fourInt == 2) {
                    iList.add(1 + shifter);iList.add(10 + shifter);iList.add(11 + shifter);
                } else if ( fourInt == 3) {
                    iList.add(2 + shifter);iList.add(9 + shifter);iList.add(17 + shifter);
                } else if ( fourInt == 6) {
                    iList.add(2 + shifter);iList.add(3 + shifter);iList.add(9 + shifter);
                } else if ( fourInt == 7) {
                    iList.add(1 + shifter);iList.add(10 + shifter);iList.add(18 + shifter);
                }
            } else if (oneString == 'i') {
                iList.add(1 + shifter);
            }
        }
        Collections.sort(iList);
        return iList;
    }

    static List jBoardChecker (String placement) {
        int stringLength = placement.length();
        List<Integer> jList = new ArrayList<>();
        for (int a = 0; a < stringLength; a +=4) {
            char oneString = placement.charAt(a); //Which shape
            char twoString = placement.charAt(a + 1); //Which Column
            char threeString = placement.charAt(a + 2); // Which Row
            char fourString = placement.charAt(a + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString;
            int fourInt = fourString - '0';
            int shifter = (twoInt - 1) + ((threeInt - 65) * 8);

            if (oneString == 'a' || oneString == 'b' || oneString == 'e' || oneString == 'f' || oneString == 'g' || oneString == 'h') {
                String send = "" + oneString + twoString + threeString + fourString;
                jList.addAll(PiecePlacer.piecePlacer(send));
            } else if (oneString == 'c') {
                if (fourInt == 0 || fourInt == 4 ) {
                    jList.add(1 + shifter);jList.add(3 + shifter);jList.add(4 + shifter);
                } else if (fourInt == 1 || fourInt == 5) {
                    jList.add(1 + shifter);jList.add(17 + shifter);jList.add(25 + shifter);
                } else if (fourInt == 2 || fourInt == 6) {
                    jList.add(1 + shifter);jList.add(2 + shifter);jList.add(4 + shifter);
                } else if (fourInt == 3 || fourInt == 7) {
                    jList.add(1 + shifter);jList.add(9 + shifter);jList.add(25 + shifter);
                }
            } else if (oneString == 'd') {
                if (fourInt == 0) {
                    jList.add(1 + shifter);jList.add(2 + shifter);jList.add(3 + shifter);
                } else if (fourInt == 1) {
                    jList.add(2 + shifter);jList.add(10 + shifter);jList.add(18 + shifter);
                } else if (fourInt == 2) {
                    jList.add(9 + shifter);jList.add(10 + shifter);jList.add(11 + shifter);
                } else if (fourInt == 3) {
                    jList.add(1 + shifter);jList.add(9 + shifter);jList.add(17 + shifter);
                } else if (fourInt == 4) {
                    jList.add(9 + shifter);jList.add(10 + shifter);jList.add(11 + shifter);
                } else if (fourInt == 5) {
                    jList.add(1 + shifter);jList.add(9 + shifter);jList.add(17 + shifter);
                } else if (fourInt == 6) {
                    jList.add(1 + shifter);jList.add(2 + shifter);jList.add(3 + shifter);
                } else if (fourInt == 7) {
                    jList.add(2 + shifter);jList.add(10 + shifter);jList.add(18 + shifter);
                }
            } else if (oneString == 'j') {
                jList.add(1 + shifter);
            }
        }

        Collections.sort(jList);
        return jList;

    }

    static List kBoardChecker (String placement) {
        int stringLength = placement.length();
        List<Integer> kList = new ArrayList<>();

        for (int a = 0; a < stringLength; a +=4) {
            char oneString = placement.charAt(a); //Which shape
            char twoString = placement.charAt(a + 1); //Which Column
            char threeString = placement.charAt(a + 2); // Which Row
            char fourString = placement.charAt(a + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString;
            int fourInt = fourString - '0';
            int shifter = (twoInt - 1) + ((threeInt - 65) * 8);

            if (oneString == 'a' || oneString == 'b' || oneString == 'c' || oneString == 'd' || oneString == 'g' || oneString == 'h') {
                String send = "" + oneString + twoString + threeString + fourString;
                kList.addAll(PiecePlacer.piecePlacer(send));
            } else if (oneString == 'e') {
                if (fourInt == 0 ) {
                    kList.add(1 + shifter);
                } else if (fourInt == 1 ) {
                    kList.add(2 + shifter);
                } else if (fourInt == 2 ) {
                    kList.add(10 + shifter);
                } else if (fourInt == 3 ) {
                    kList.add(9 + shifter);
                } else if ( fourInt == 4) {
                    kList.add(9 + shifter);
                } else if ( fourInt == 5) {
                    kList.add(1 + shifter);
                } else if (fourInt == 6) {
                    kList.add(2 + shifter);
                } else if ( fourInt == 7) {
                    kList.add(10 + shifter);
                }
            } else if (oneString == 'f') {
                if (fourInt == 0 ) {
                    kList.add(1 + shifter);kList.add(2 + shifter);
                } else if (fourInt == 1 ) {
                    kList.add(2 + shifter);kList.add(10 + shifter);
                } else if (fourInt == 2 ) {
                    kList.add(10 + shifter);kList.add(11 + shifter);
                } else if (fourInt == 3 ) {
                    kList.add(9 + shifter);kList.add(17 + shifter);
                } else if ( fourInt == 4) {
                    kList.add(9 + shifter);kList.add(10 + shifter);
                } else if ( fourInt == 5) {
                    kList.add(1 + shifter);kList.add(9 + shifter);
                } else if (fourInt == 6) {
                    kList.add(2 + shifter);kList.add(3 + shifter);
                } else if (fourInt == 7) {
                    kList.add(10 + shifter);kList.add(18 + shifter);
                }
            } else if (oneString == 'k') {
                kList.add(1 + shifter);
            }


        }
        Collections.sort(kList);
        return kList;
    }

    static List lBoardChecker(String placement) {
        int stringLength = placement.length();
        List<Integer> lList = new ArrayList<>();

        for (int a = 0; a < stringLength; a +=4) {
            char oneString = placement.charAt(a); //Which shape
            char twoString = placement.charAt(a + 1); //Which Column
            char threeString = placement.charAt(a + 2); // Which Row
            char fourString = placement.charAt(a + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString;
            int fourInt = fourString - '0';
            int shifter = (twoInt - 1) + ((threeInt - 65) * 8);

            if (oneString == 'a' || oneString == 'b' || oneString == 'c' || oneString == 'd' || oneString == 'e' || oneString == 'f') {
                String send = "" + oneString + twoString + threeString + fourString;
                lList.addAll(PiecePlacer.piecePlacer(send));
            } else if (oneString == 'g') {
                if (fourInt == 0) {
                    lList.add(10 + shifter);lList.add(11 + shifter);
                } else if (fourInt == 1) {
                    lList.add(10 + shifter);lList.add(18 + shifter);
                } else if (fourInt == 2) {
                    lList.add(9 + shifter);lList.add(10 + shifter);
                } else if (fourInt == 3) {
                    lList.add(2 + shifter);lList.add(10 + shifter);
                } else if (fourInt == 4) {
                    lList.add(10 + shifter);lList.add(11 + shifter);
                } else if (fourInt == 5) {
                    lList.add(10 + shifter);lList.add(18 + shifter);
                } else if (fourInt == 6) {
                    lList.add(9 + shifter);lList.add(10 + shifter);
                } else if (fourInt == 7) {
                    lList.add(2 + shifter);lList.add(10 + shifter);
                }

            } else if (oneString == 'h') {
                if (fourInt == 0 || fourInt == 4 ) {
                    lList.add(2 + shifter);lList.add(3 + shifter);
                } else if (fourInt == 1 || fourInt == 5 ) {
                    lList.add(9 + shifter);lList.add(17 + shifter);
                }else if (fourInt == 2 || fourInt == 6) {
                    lList.add(1 + shifter);lList.add(2 + shifter);
                } else if (fourInt == 3 ||  fourInt == 7) {
                    lList.add(1 + shifter);lList.add(9 + shifter);
                }
            } else if (oneString == 'l') {
                lList.add(1 + shifter);
            }
        }
        Collections.sort(lList);
        return lList;

    }
}
