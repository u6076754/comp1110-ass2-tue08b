package comp1110.ass2;

import java.util.ArrayList;
import java.util.List;

//Peter.Cominos
//The class is used to check if the the piece is on the board through assessing its boundaries

public class OnBoard {

    static boolean isPlacementOnBoard(String placement) { //need to check each set of four letter, we have established that the string is already well formed
        int stringLength = placement.length();
        List<Integer> stuff = new ArrayList<>();

        //looping over each set of four to asset each piece individually
        for (int i = 0; i < stringLength ; i += 4) {
            char oneString = placement.charAt(i); //Which shape
            char twoString = placement.charAt(i + 1); //Which Column
            char threeString = placement.charAt(i + 2); // Which Row
            char fourString = placement.charAt(i + 3); //Which Orientation
            int twoInt = twoString - '0';
            int threeInt = (int) threeString -65;
            int fourInt = fourString - '0';

            // Testing each individual piece as to whether it passes the requirements for being on the board, each piece has one or two rotational possibilities
            //A square is drawn using the top most piece representing the row anf the left most piece representing the column, together these create a square that must lie within the bounds of the board
            if (oneString == 'a') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if ((twoInt + 2) > 8 || (threeInt + 1) > 3) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) {
                    if (twoInt + 1 > 8 || threeInt + 2 > 3) { stuff.add(1); } }
            } if (oneString == 'b') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if (twoInt + 2 > 8 || threeInt + 1 > 3) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (twoInt + 1 > 8 || threeInt + 2 > 3) { stuff.add(1); } }
            } if (oneString == 'c') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if (twoInt + 3 > 8) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (threeInt + 3 > 3) { stuff.add(1); } }
            } else if (oneString == 'd') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if (twoInt + 2 > 8 || threeInt + 1 > 3) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (twoInt + 1 > 8 || threeInt + 2 > 3) { stuff.add(1); } }
            }if (oneString == 'e') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6 || fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (twoInt + 1 > 8 || threeInt + 1 > 3) { stuff.add(1); } }
            } else if (oneString == 'f') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if (twoInt + 2 > 8 || threeInt + 1 > 3) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (twoInt + 1 > 8 || threeInt + 2 > 3) { stuff.add(1); } }
            } else if (oneString == 'g') {
                if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6 || fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (twoInt + 2 > 8 || threeInt + 2 > 3) { stuff.add(1); } }
            } else if (oneString == 'h') { if (fourInt == 0 || fourInt == 2 || fourInt == 4 || fourInt == 6) { if (twoInt + 2 > 8) { stuff.add(1); } }
                if (fourInt == 1 || fourInt == 3 || fourInt == 5 || fourInt == 7) { if (threeInt + 2 > 3) { stuff.add(1); } }
            } else if (oneString == 'i' || oneString == 'j' || oneString =='k' || oneString == 'l') {
                if (twoInt > 8 || twoInt < 0) { stuff.add(1); } else if (threeInt > 3 || threeInt < 0) { stuff.add(1); }
            }}


        if (stuff.size() > 0) {
            return false;
        } else return true;


    }
}
