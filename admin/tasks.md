# COMP1110 Assignment 2 Group Tasks

For each task or sub-task, record who is responsible, the deadline, and any dependencies.
Use the entries below as an example.

## Week 4

Names: Peter Cominos
UID: u6076754
roles: TBA
weekly meeting time: Wednesday 5:00pm
contact details: Facebook

Names: Juyeon Lee
UID: u4398602
roles: TBA
weekly meeting time: Wednesday 5:00pm
contact details: Facebook

Everyone: create application skeleton - meeting 1730 Wednesday 15 August

## Week 5

Zhang San: Task 3 isPiecePlacementWellFormed - 21 Aug

Jane Bloggs: Task 4 getNeighbours - 23 Aug

Erika Mustermann: Task 6 getViablePiecePlacements - 24 Aug (depends on Task 3)

## Week 6

...

## Mid-Semester Break

## Week 7

## Week 8

## Week 9

## Week 10

## Week 11
