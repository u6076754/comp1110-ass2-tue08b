Reviewer: Peter Cominos (u6076754)
Component: Task 4 Viewer
Author: Amelia Poetter  (u6054910)

Review Comments:

1.      Code complies and viewer is shown on screen as expected, this is a reasonably good implementation of the task at hand
2.      Placement that is being drawn does not do a singular piece at a time but rather multiple pieces, and need to have a button which will remove the pieces when needed
2.a     The placement that is being drawn also need to be able to rotate the piece as needed, rather then just giving the most basic piece
3.      The code is reasonably documented but need more attention through-out the code, rather than the beginning and then end to ensure clarity for reviewers
4.      The class and method structure is appropriate and resembles the level of skill we have learnt thus far in the course, but will need to be slowly updated at the term continues to progress
5.      The correct java conventions are followed throughout, but once again need further commenting to ensure that it is easily readable by a third part reviewer
6.      Updates that may be useful are the commenting out of pieces of code that are no longer being used to help with confusion when writing and reading the code as a reviewer