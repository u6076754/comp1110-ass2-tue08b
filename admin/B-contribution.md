We declare that the work toward our submission of Stage B was distributed among the group members as follows:

* u6076754 33
* u4398602 33
* u6054910 33

Signed: Peter Demetrius Cominos (u6076754), Ju Yeon Lee  (u4398602), and Amelia Poetter  (u6054910)
