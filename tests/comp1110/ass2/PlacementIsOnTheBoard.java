package comp1110.ass2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
//peter.cominos
public class PlacementIsOnTheBoard {


    @Test
    public void testIsOnTheBoardSingularPiece() {
        assertTrue(OnBoard.isPlacementOnBoard("a7A7"));
        assertTrue(OnBoard.isPlacementOnBoard("b6A7"));
        assertTrue(OnBoard.isPlacementOnBoard("c1A3"));
        assertTrue(OnBoard.isPlacementOnBoard("d2A6"));
        assertTrue(OnBoard.isPlacementOnBoard("e2C3"));
        assertTrue(OnBoard.isPlacementOnBoard("f3C4"));
        assertTrue(OnBoard.isPlacementOnBoard("g4A7"));
        assertTrue(OnBoard.isPlacementOnBoard("h6D0"));
        assertTrue(OnBoard.isPlacementOnBoard("i6B0"));
        assertTrue(OnBoard.isPlacementOnBoard("j2B0"));
        assertTrue(OnBoard.isPlacementOnBoard("j1C0"));
        assertTrue(OnBoard.isPlacementOnBoard("k3C0"));
        assertTrue(OnBoard.isPlacementOnBoard("l4B0"));
        assertTrue(OnBoard.isPlacementOnBoard("l5C0"));

        assertFalse(OnBoard.isPlacementOnBoard("a7D7"));
        assertFalse(OnBoard.isPlacementOnBoard("d8D1"));
        assertFalse(OnBoard.isPlacementOnBoard("f7A4"));

    }


    @Test
    public void testIsOnTheBoardMultiplePiece(){
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0"));
        assertTrue(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));

        assertFalse(OnBoard.isPlacementOnBoard("a7D7b6A7"));
        assertFalse(OnBoard.isPlacementOnBoard("a7A7d8D1"));
        assertFalse(OnBoard.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f8C4"));
        assertFalse(OnBoard.isPlacementOnBoard("a7D7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));
        assertFalse(OnBoard.isPlacementOnBoard("a7A7b6D7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));
    }



}
