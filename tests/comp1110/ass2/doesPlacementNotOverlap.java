package comp1110.ass2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
 // peter.cominos
public class doesPlacementNotOverlap {


    @Test
    public void testIsOnTheBoardSingularPiece() {
        assertTrue(TwistGame.doesPlacementNotOverlap("a7A7"));
        assertTrue(TwistGame.doesPlacementNotOverlap("b6A7"));
        assertTrue(TwistGame.doesPlacementNotOverlap("c1A3"));
        assertTrue(TwistGame.doesPlacementNotOverlap("d2A6"));
        assertTrue(TwistGame.doesPlacementNotOverlap("e2C3"));
        assertTrue(TwistGame.doesPlacementNotOverlap("f3C4"));
        assertTrue(TwistGame.doesPlacementNotOverlap("g4A7"));
        assertTrue(TwistGame.doesPlacementNotOverlap("h6D0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("i6B0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("j2B0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("j1C0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("k3C0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("l4B0"));
        assertTrue(TwistGame.doesPlacementNotOverlap("l5C0"));

        assertFalse(TwistGame.doesPlacementNotOverlap("a7A7b7A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));
        assertFalse(TwistGame.doesPlacementNotOverlap("a7A7b7A7"));


    }


    @Test
    public void testIsOnTheBoardMultiplePiece(){

        assertTrue(TwistGame.doesPlacementNotOverlap("a7A7b6A7c1A3"));
        assertTrue(TwistGame.doesPlacementNotOverlap("a7A7b6A7c1A3d2A6"));


        assertFalse(TwistGame.doesPlacementNotOverlap("a7A7b7A7"));
        assertFalse(TwistGame.doesPlacementNotOverlap("a7A7b7A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));
/*
        assertFalse(TwistGame.isPlacementOnBoard("a7D7b6A7"));
        assertFalse(TwistGame.isPlacementOnBoard("a7A7d8D1"));
        assertFalse(TwistGame.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f8C4")); */
    }



}
