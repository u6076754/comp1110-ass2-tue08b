package comp1110.ass2;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

// peter.cominos
public class arePegsCorret {


   @Test
   public void testIsOnTheBoardSingularPiece() {
       assertTrue(TwistGame.arePegsCorrect("a7A7"));
       assertTrue(TwistGame.arePegsCorrect("b6A7"));
       assertTrue(TwistGame.arePegsCorrect("c1A3"));
       assertTrue(TwistGame.arePegsCorrect("d2A6"));
       assertTrue(TwistGame.arePegsCorrect("e2C3"));
       assertTrue(TwistGame.arePegsCorrect("f3C4"));
       assertTrue(TwistGame.arePegsCorrect("g4A7"));
       assertTrue(TwistGame.arePegsCorrect("h6D0"));
       assertTrue(TwistGame.arePegsCorrect("i6B0"));
       assertTrue(TwistGame.arePegsCorrect("j2B0"));
       assertTrue(TwistGame.arePegsCorrect("j1C0"));
       assertTrue(TwistGame.arePegsCorrect("k3C0"));
       assertTrue(TwistGame.arePegsCorrect("l4B0"));
       assertTrue(TwistGame.arePegsCorrect("l5C0"));




   }


   @Test
   public void testIsOnTheBoardMultiplePiece(){
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7"));
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7c1A3"));
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7c1A3d2A6"));
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7c1A3d2A6e2C3"));
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));

       assertFalse(TwistGame.arePegsCorrect("a7A7j8A0"));
       assertTrue(TwistGame.arePegsCorrect("a7A7b6A7c1A3d2A6e2C3f3C4g4A7h6D0i6B0j2B0j1C0k3C0l4B0l5C0"));
/* ////
       assertFalse(TwistGame.isPlacementOnBoard("a7D7b6A7"));
       assertFalse(TwistGame.isPlacementOnBoard("a7A7d8D1"));
       assertFalse(TwistGame.isPlacementOnBoard("a7A7b6A7c1A3d2A6e2C3f8C4")); */
   }



}
